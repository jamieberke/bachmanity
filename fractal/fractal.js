// Pattern library settings

('use strict');

/* Create a new Fractal instance and export it for use elsewhere if required */
const fractal = (module.exports = require('@frctl/fractal').create());

/* Set the title of the project */
fractal.set('project.title', 'Bachmanity Component Library');

/* Tell Fractal where the components will live */
fractal.components.set('path', __dirname + '/../src/components');

/* Tell Fractal where the documentation pages will live */
fractal.docs.set('path', __dirname + '/../src/docs');

/* Specify a directory of static assets */
fractal.web.set('static.path', __dirname + '/../');

/* Set the static HTML build destination */
fractal.web.set('builder.dest', __dirname + '/../build');

fractal.components.set('default.preview', '@preview');

fractal.docs.set('statuses', {
  draft: {
    label: 'Draft',
    description: 'Work in progress.',
    color: '#FF3333'
  },
  ready: {
    label: 'Ready',
    description: 'Ready for referencing.',
    color: '#29CC29'
  }
});

fractal.components.set('statuses', {
  prototype: {
    label: 'Prototype',
    description: 'Do not implement.',
    color: '#FF3333'
  },
  wip: {
    label: 'WIP',
    description: 'Work in progress. Implement with caution.',
    color: '#FF9233'
  },
  ready: {
    label: 'Ready',
    description: 'Ready to implement.',
    color: '#29CC29'
  }
});

fractal.web.set('server.syncOptions', {
  open: !process.argv.includes('silent')
});

/**
 * List Components custom fractual command
 * @param {*} args
 * @param {*} done
 */
function listComponents(args, done) {
  const app = fractal;
  for (let item of app.components.flatten()) {
    log(`${item.handle} - ${item.status.label}`);
  }
  done();
}

// Register custom functions
fractal.cli.command('list-components', listComponents);

// Handlebars options
const hbs = require('@frctl/handlebars')({
  helpers: {
    uppercase: str => str.toUpperCase(),
    lowercase: str => str.toLowerCase(),
    boolean: (test, val) => test && val,
    json: context => JSON.stringify(context)
  }
  /* other configuration options here */
});

fractal.components.engine(
  hbs
); /* set as the default template engine for components */
fractal.docs.engine(
  hbs
); /* you can also use the same instance for documentation, if you like! */

// Theme settings

const mandelbrot = require('@frctl/mandelbrot'); // require the Mandelbrot theme module

// create a new instance with custom config options
const myCustomisedTheme = mandelbrot({
  format: 'json',
  styles: ['/themes/mandelbrot/css/default.css', '/fractal/fractal.css']
});

fractal.web.theme(myCustomisedTheme); // tell Fractal to use the configured theme by default
