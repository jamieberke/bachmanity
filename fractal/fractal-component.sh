#!/bin/bash

source ./scripts/colours.sh

if [[ -z "$1" ]]; then
  echo ${red}"You havent specified a component name"${colEnd};
  exit
fi

cd ./src/components

if [ -d "$1" ]; then
  echo ${red}"Component $1 already exists in $PWD"${colEnd};
  exit
fi

mkdir $1
cd $1

echo "{{!-- Component: $1 --}}" > $1.hbs
echo "// Component: $1 config
module.exports = {
  context: {}
};
" > $1.config.js

echo ${grn}"Component $1 created in $PWD"${colEnd}
