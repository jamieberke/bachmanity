#!/bin/bash

source ./scripts/colours.sh

if [[ -z "$1" ]]; then
  echo ${red}"You havent specified a variant name"${colEnd};
  exit
fi

cd ./src/components/

if [ ! -d "$1" ]; then
  echo ${red}"Base Component $1 does not exist in $PWD"${colEnd};
  exit
fi

cd $1

if [ -f "$1--$2.hbs" ]; then
  echo ${red}"Component $1--$2 variant already exists in $PWD"${colEnd};
  exit
fi

echo "{{!-- Component: $1--$2 --}}" > $1--$2.hbs

echo ${grn}"Component $1--$2 variant created in $PWD"${colEnd}