# Bachmanity

This project is an out of the box development/build environment which supports the following:

* Automated pattern library using Fractal library (https://fractal.build/)
* Automated visual regression using BackstopJS (https://github.com/garris/BackstopJS)
* Automated accessibility testing using Axe-Core (https://github.com/dequelabs/axe-core)
* Out of the box Boostrap/jQuery integration for quick creation of Fractal library component examples (./src/components)
* Angular 1.5.8 for Fractal Angular example at `./src/components/product-item`
* Webpack build for development/production
* ES6/ES7 support with Babel transpiling
* Development ready commit hooks
* `SCSS` support
* `ESLint` for JavaScript code quality
* `StyleLint` for CSS/SCSS code quality
* `ImageLint`/`img-loader` for image minification

## Dependencies

* Node - v9.4.0
* NPM - v5.7.1

Once you have the above dependencies please run `npm install` to start getting setup on the environment.

## Fractal Component Library

Fractal has been used as the component pattern library. Automated component creation has been added to the environment to reduce the manual grunt work involved with setting up the components and keeping consistency accross all project components. To run the Fractal server run `npm start` which will also run the Webpack development build. To start the Fractal server manually without the Webpack build you can use `npm run fractal`.

To create new Fractal components you can use the `npm run fractal-component $1` where `$1` is the component name. This command will automatically create your component folder structure under `./src/components` with the following files:

* `$1.config.js` - The config file for the component. This allows you to set component contexts and any variants such as `button--success`
* `$1.hbs` - This is the component template file which uses Handlebars syntax. From here, you can use component context properties and any related Handlebars syntax to generate your markup.

To create new Fractal component variants you can use the `npm run fractal-variant $1 $1` where `$1` is the component name and `$2` is its variant. This command will automatically create your component variant inside the relevant component folder under `./src/components/$1`. The following file will be created:

* `$1--$2.hbs` - This is the component variant template file which uses Handlebars syntax. From here, you can use component context properties and any related Handlebars syntax to generate your markup.

Fractal components within this project can be configured using three states, `WIP`, `Prototype` and `Ready` to give visibility to the rest of the development/testing team as to what state a component is in and if it is ready to be tested.

If you need to add any Handlebars helpers, you can add these inside `./fractal/fractal.js` near the bottom of the file. This development environment supports the following helpers out of the box:

* `uppercase` - Converts strings to uppercase
* `lowercase` - Converts strings to lowercase
* `boolean` - returns a value if the test returns true
* `json` - Converts a string to JSON format

**Notes**:

Other useful Fractal commands:

* `npm run fractal-list-components` - Lists the components from the Fractal component library
* `npm run fractal-errors` - Checks the Fractal pattern library for compilation errors

## BackstopJS

BackstopJS is a visual regression testing tool for web apps. It uses a Backstop config file to generate the tests. Writing Backstop tests for each component can become quite monotonous and cumbersome, so this process has been completely automated creating the configs on the fly for the relevant components.

BackstopJS works by taking screenshots of the Fractal component(s) against image references. For this example the component screenshots point at the preview of each component e.g. (http://localhost:3004/components/preview/blockquote--default). These pages display the components in their simplest form wrapped inside a container, so the reference designs would need to match these as close as possible to ensure Backstop test are ran accurately.

Another idea for running Backstop tests is to have the image references as a pre-production environment where the pattern library exists. For example, you could have the test image as the local development branch (http://localhost:3004/components/preview/blockquote--default) snapshot and the image reference as a pre-production environment (http://pre-production.com/components/preview/blockquote--default).

To run a test against an indivudal component use `npm run backstop-test $1` where `$1` is the component name. Once finished, a test report will open with the results.

### BackstopJS Happy Path

Being a visual regression tool, Backstop would work extremely well in a CI pipeline where testers can test components against pre-production (live) and block SYSTEST/UAT deployments if a live signed off component no longer matches its reference when progressing through the test environments.

A visual UI has also been created to match this process where testers/developers can test against a number of component(s) easily. To run this `nodejs` application please run the `npm run backstop` command.

**Notes**:

* Backstops visual UI uses the Fractal component library to create its component list. If your component variant is not appearing then it may not be added as a component variant inside its `config` file.
* By default, after each test runs, the Backstop image references will be cleared to stop the Backstop data folder getting too large. If you ever need to manually clean it you can use the `npm run backstop-clean` command.

## Axe core

Axe core is a test library for testing against accessibility. Axe core supports integrations with most testing frameworks including Jasmine, Selenium etc. For the purpose of this environment, it has been integrated using Selenium (https://www.seleniumhq.org/docs/03_webdriver.jsp) & Axe (https://github.com/dequelabs/axe-webdriverjs) Webdriver. In general, the tests can be written manually per component/page. However, testing accessibility should really be done at page level to allow full testing of page structure including heading orders, meta tags etc.

The tests take the HTML markup from the URL and runs it through a the Axe-core and Selenium Webdrivers.

### Axe-Core Jenkins Integration

Axe core can be integrated into Jenkins and the test suite can be ran against a number of URL paths. To configure these paths you can add to the `path config` array inside `/axe-core/paths.js`. By default, the test suite will be ran against each path prefixed with a base environment URL variable which can be set by using the `npm config set bachmanity:envUrl $1` in the terminal where `$1` is the base environment URL. Once you have configured the paths, you can run the tests by running `npm run axe`.

The results of running this command will output a results file for each test into `/axe-core/results`.

**Notes**:

* If you ever need to manually clean the results folder you can use the `npm run axe-clean` command.
* To disabled specific Accessibility rules you can access the axe config inside `/axe-core/axe.config.js` and set the `enabled` property to `false` for specific properties.

## Webpack

Webpack has been added as part of the development/build environment. Webpack acts as a module bundler to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging just about any resource or asset.

To run the development build of Webpack use `npm start`. This will start the Fractal component library and run the Webpack build in development mode.

To run the production ready build you can use `npm build`.

## Releases

An automated release tool has been added to this development environment. This automatically updates the version of the package JSON, updates the automated `CHANGELOG` file at `./CHANGELOG.md`, creates the relevant release tag and creates a related commit for the release update.

To create a release you can use the `npm version patch|minor|major` command where the following rules apply:

Assuming the current version is `1.0.0`:

* `npm version patch` increments to `1.0.1` - Mostly fixing bugs/hotfixes
* `npm version minor` increments to `1.1.0` - Implementing new features in a backward-compatible way such as smaller components/applications
* `npm version major` increments to `2.0.0` - Implementing larger new features including larger apps that include new/changed API's.

## NPM scripts

| **Command**                           | **Description**                                                                                                                                                             |
| ------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `npm run axe`                         | Runs the axe test suite for accessibility testing using the `/axe-core/paths.js` config. See [Axe-Core-Jenkins-Integration](#axe-core-jenkins-integration) for more details |
| `npm run axe-clean`                   | Clears the axe-core results folder and files                                                                                                                                |
| `npm run backstop`                    | Runs the backstop server for visual regression testing                                                                                                                      |
| `npm run backstop-clean`              | Clears the backstop reference and image folders                                                                                                                             |
| `npm run backstop-test $1`            | Runs the visual regression suite based on the Fractal component library using `$1` as the component/variant name                                                            |
| `npm run build`                       | Generates the Webpack production build                                                                                                                                      |
| `npm config set bachmanity:envUrl $1` | Sets the envUrl for use with `npm run axe` command                                                                                                                          |
| `npm run fractal`                     | Starts the Fractal server pattern library                                                                                                                                   |
| `npm run fractal-component $1`        | Creates a Fractal base component structure using `$1` as the base component name                                                                                            |
| `npm run fractal-list-components`     | Lists the components from the Fractal component library                                                                                                                     |
| `npm run fractal-variant $1 $2`       | Creates a Fractal variant component structure using `$1` as an existing base component and `$2` as its variant name                                                         |
| `npm start`                           | Starts the Webpack build and opens the pattern library                                                                                                                      |
| `npm version $1`                      | Runs the automated release scripts where `$1` is the release type (patch, minor or major)                                                                                   |
