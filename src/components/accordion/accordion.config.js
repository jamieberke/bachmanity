// Component: accordion config
const itemCount = 3;
const accordionData = [];

for (let i = 1; i <= itemCount; i++) {
  accordionData.push({
    title: `Name ${i}`,
    data:
      'voluptatem libero blanditiis voluptate harum sint et debitis maiores repellat debitis maxime reprehenderit esse autem incidunt pariatur est iure illo est qui consequatur et quia impedit ut quas et non et sint nam et sed sit ab suscipit hic officia harum impedit recusandae exercitationem et quo enim doloribus enim ut aut et eos asperiores et quod vel alias quisquam earum vitae dolorum officia nemo sunt ut cum enim dolores maxime voluptatum nisi accusantium quo rerum ut excepturi quaerat vitae eum autem qui non eum est quasi quis quasi perspiciatis aspernatur ut consequatur sit eveniet nam hic necessitatibus harum quis molestiae'
  });
}

module.exports = {
  variants: [
    {
      name: 'default'
    }
  ],
  context: {
    accordionItems: accordionData
  }
};
