import angular from 'angular';

const app = angular.module('myApp', []);

app.directive('productItem', () => {
  return {
    restrict: 'A',
    transclude: false,
    template: `
      <h2>Product name:</h2>
      <p>{{product.name}}</p>
      <h2>Product material:</h2>
      <p>Product material: {{product.material}}</p>
      <h2>Product colour:</h2> 
      <p>{{product.colour}}</p>
      <h2>Product description:</h2> 
      <p>{{product.description}}</p>
    `,
    scope: {
      product: '='
    }
  };
});
