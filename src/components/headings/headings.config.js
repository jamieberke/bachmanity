// Component: heading config
module.exports = {
  variants: [
    {
      name: 'default',
      label: 'One',
      context: {
        variant: '1',
        text: 'Heading one'
      }
    },
    {
      name: 'two',
      label: 'Two',
      context: {
        variant: '2',
        text: 'Heading two'
      }
    },
    {
      name: 'three',
      label: 'Three',
      context: {
        variant: '3',
        text: 'Heading three'
      }
    },
    {
      name: 'four',
      label: 'Four',
      context: {
        variant: '4',
        text: 'Heading four'
      }
    },
    {
      name: 'five',
      label: 'Five',
      context: {
        variant: '5',
        text: 'Heading five'
      }
    },
    {
      name: 'six',
      label: 'Six',
      context: {
        variant: '6',
        text: 'Heading six'
      },
    }
  ]
};
