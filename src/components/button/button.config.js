// Component: button config
module.exports = {
  variants: [
    {
      name: 'default',
      label: 'Primary',
      context: {
        title: 'Primary',
        style: 'primary',
        variant: 'button'
      }
    },
    {
      name: 'secondary',
      context: {
        title: 'Secondary',
        style: 'secondary',
        variant: 'button'
      }
    },
    {
      name: 'success',
      context: {
        title: 'Success',
        style: 'success',
        variant: 'button'
      }
    },
    {
      name: 'danger',
      context: {
        title: 'Danger',
        style: 'danger',
        variant: 'button'
      }
    },
    {
      name: 'warning',
      context: {
        title: 'Warning',
        style: 'warning',
        variant: 'button'
      }
    },
    {
      name: 'info',
      context: {
        title: 'Info',
        style: 'info',
        variant: 'button'
      }
    },
    {
      name: 'light',
      context: {
        title: 'Light',
        style: 'light',
        variant: 'button'
      }
    },
    {
      name: 'dark',
      context: {
        title: 'Dark',
        style: 'dark',
        variant: 'button'
      }
    },
    {
      name: 'link',
      context: {
        title: 'Link',
        style: 'link',
        variant: 'button'
      }
    }
  ]
};
