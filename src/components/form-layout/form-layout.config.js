// Component: form-layout config
module.exports = {
  variants: [
    {
      name: 'default',
      label: 'Stacked',
      context: {
        firstName: {
          label: {
            text: 'First name'
          },
          input: {
            id: 'firstname',
            type: 'text',
            placeholder: 'Enter first name', 
            required: true
          }
        },
        lastName: {
          label: {
            text: 'Last name'
          },
          input: {
            id: 'lastname',
            type: 'text',
            placeholder: 'Enter last name', 
            required: true
          }
        },
        submit: {
          title: 'Submit',
          style: 'primary',
          type: 'submit'
        }
      }
    },
    {
      name: 'grid',
      context: {
        email: {
          label: {
            text: 'Email'
          },
          input: {
            id: 'email',
            type: 'email',
            placeholder: 'Enter email', 
            required: true
          }
        },
        password: {
          label: {
            text: 'Password'
          },
          input: {
            id: 'password',
            type: 'password',
            placeholder: 'Enter password', 
            required: true
          }
        },
        address: {
          label: {
            text: 'Address'
          },
          input: {
            id: 'address',
            type: 'text',
            placeholder: 'Enter address', 
            required: true
          }
        },
        city: {
          label: {
            text: 'City'
          },
          input: {
            id: 'city',
            type: 'text',
            placeholder: 'Enter city', 
            required: true
          }
        },
        state: {
          label: {
            text: 'State'
          },
          input: {
            id: 'state',
            placeholder: 'Please select an options', 
            required: true,
            options: ['Chelsea', 'Liverpool', 'Manchester', 'Watford']
          }
        },
        zip: {
          label: {
            text: 'Zip'
          },
          input: {
            id: 'zip',
            type: 'zip',
            placeholder: 'Enter Zip', 
            required: true
          }
        },
        check: {
          label: {
            text: 'Check me?'
          },
          input: {
            id: 'check',
            required: true
          }
        },
        submit: {
          title: 'Submit',
          style: 'primary',
          type: 'submit'
        }
      }
    },
    {
      name: 'inline',
      context: {
        name: {
          label: {
            text: 'Name',
            inline: true
          },
          input: {
            id: 'name',
            type: 'text',
            placeholder: 'Enter name', 
            required: true
          }
        },
        username: {
          label: {
            text: 'Username',
            inline: true
          },
          input: {
            id: 'username',
            type: 'text',
            placeholder: 'Enter username', 
            required: true,
            prefix: '&#64;'
          }
        },
        check: {
          label: {
            text: 'Remember me'
          },
          input: {
            id: 'check',
            required: true
          }
        },
        submit: {
          title: 'Go',
          style: 'primary',
          type: 'submit'
        }
      }
    }
  ]
};
