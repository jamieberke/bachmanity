// Component: form-components config
module.exports = {
  variants: [
    {
      name: 'default',
      label: 'Input',
      context: {
        label: {
          text: 'Input example',
          inline: false
        },
        input: {
          id: 'inputId',
          type: 'input',
          placeholder: 'I am a placeholder',
          required: false,
          disabled: false,
          prefix: ''
        }
      }
    },
    {
      name: 'select',
      context: {
        label: {
          text: 'Select example',
          inline: false
        },
        input: {
          id: 'selectId',
          options: ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'],
          disabled: false,
          required: false,
          placeholder: 'Please select an option',
          multiple: false
        }
      }
    },
    {
      name: 'checkbox',
      context: {
        label: {
          text: 'Checkbox example',
          inline: false
        },
        input: {
          id: 'checkboxId',
          required: false,
          inline: false,
          disabled: false
        }
      }
    },
    {
      name: 'radio',
      context: {
        label: {
          text: 'Radio example',
          inline: false
        },
        input: {
          id: 'radioId',
          required: false,
          inline: false,
          disabled: false
        }
      }
    },
    {
      name: 'text-area',
      context: {
        label: {
          text: 'Text Area example',
          inline: false
        },
        input: {
          id: 'textAreaId',
          rows: '3',
          placeholder: 'I am a placeholder',
          required: false,
          disabled: false
        }
      }
    }
  ]
};
