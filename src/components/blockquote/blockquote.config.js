// Component: blockquote config
module.exports = {
  variants: [
    {
      name: 'default',
      context: {
        quote: {
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.'
        },
      }
    },
    {
      name: 'referenced',
      label: 'Referenced',
      context: {
        quote: {
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.'
        },
        footer: {
          title: 'Source Title'
        }
      }
    }
  ]
};
