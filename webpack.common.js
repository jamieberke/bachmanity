const webpack = require('webpack');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const gitRevisionPlugin = new GitRevisionPlugin();
const execSync = require('child_process').execSync;
const currentBranch = execSync('git symbolic-ref --short HEAD');

module.exports = {
  context: __dirname,
  entry: './src/index.js',
  output: {
    path: __dirname + '/public',
    filename: 'bachmanity.min.js'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        common: {
          chunks: 'initial',
          enforce: true,
          filename: '[name].min.js',
          name: 'commons',
          test: /[\\/]js[\\/]/,
          reuseExistingChunk: true
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(jpg|png|ttf|eot|woff|woff2|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 5000
            }
          },
          {
            loader: 'img-loader',
            options: {
              progressive: true,
              minimize: true
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: 'html-loader'
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin({
      banner: currentBranch + gitRevisionPlugin.commithash()
    })
  ]
};
