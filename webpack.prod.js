const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const execSync = require('child_process').execSync;

let cleanBuild = true;

if (!cleanBuild) {
  console.error(
    '\x1b[31m',
    'Build aborted due to uncommitted files',
    '\x1b[0m'
  );
  process.exit(1);
}
try {
  execSync(`rm -r ./public/*`);
} catch (e) {
  console.log('Clearing public folder failed');
}

module.exports = merge(common, {});
