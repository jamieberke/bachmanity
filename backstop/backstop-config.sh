#!/bin/bash

source ./scripts/colours.sh
source ./scripts/paths.sh
source ./backstop/backstop-component.sh


if [ ! -f ${backstopConfigPath} ]; then
  touch ${backstopConfigPath}
  else
  rm ${backstopConfigPath}
  touch ${backstopConfigPath}
fi

if [[ $1 = *"--"* ]]; then
  variantConfig $1
  else
  componentConfig $1
fi