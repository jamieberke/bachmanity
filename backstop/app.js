window.onload = () => {
  const selectAll = document.getElementsByClassName('js-toggle-select-all')[0];
  const form = document.getElementsByClassName('js-axe-form')[0];
  const checkboxes = document.getElementsByClassName('js-component-checkbox');
  const formError = document.getElementsByClassName('js-form-error')[0];
  const formLoader = document.getElementsByClassName('js-form-loader')[0];
  const container = document.getElementsByClassName('js-container')[0];

  selectAll.addEventListener('click', () => {
    for (let i = 0, n = checkboxes.length; i < n; i++) {
      checkboxes[i].checked = !checkboxes[i].checked;
    }
    if (checkboxes[0].checked) {
      selectAll.textContent = 'Deselect All';
    } else {
      selectAll.textContent = 'Select All';
    }
  });

  form.addEventListener(
    'submit',
    e => {
      for (let i = 0, n = checkboxes.length; i < n; i++) {
        if (checkboxes[i].checked) {
          formError.textContent = '';
          formLoader.className += formLoader.className ? ' d-block' : 'd-block';
          container.className += container.className ? ' loading' : 'loading';
          return;
        }
      }
      e.preventDefault();
      formError.textContent = 'Please select at least one component';
    },
    false
  );
};
