const colors = require('colors');
const httpreq = require('httpreq');
const endpoints = require('./../scripts/endpoints');
const backstopConfig = require('./backstop-config.js');
const backstop = require('backstopjs');
const requireGlob = require('require-glob');

function isBackstopConfigEmpty() {
  if (
    Object.keys(backstopConfig).length === 0 &&
    backstopConfig.constructor === Object
  ) {
    return true;
  }
  return false;
}

function backstopTest(scenarios, callback) {
  return backstop('test', {
    config: require('./backstop-base.config.js')({
      report: (!callback && ['browser']) || ['CI'],
      scenarios: scenarios || backstopConfig.scenarios
    })
  })
    .then(() => {
      callback();
    })
    .catch(() => {
      callback();
    });
}

function backstopRef(scenarios) {
  return backstop('reference', {
    config: require('./backstop-base.config.js')({
      scenarios: scenarios || backstopConfig.scenarios
    })
  });
}

module.exports.getFractalComponents = callback => {
  let variantsList = [];
  requireGlob(['./../src/components/**/*.config.js']).then(modules => {
    for (const key in modules) {
      if (Object.prototype.hasOwnProperty.call(modules, key)) {
        const formattedKey = key.replace(/([A-Z])/g, '-$1').toLowerCase();
        const variants = modules[key][`${key}Config`].variants;
        for (let i = 0; i < variants.length; i++) {
          variantsList.push(`${formattedKey}--${variants[i].name}`);
        }
      }
    }
    (callback && variantsList && callback(variantsList)) || variantsList;
  });
};

module.exports.backstopRun = (scenarios, callback) => {
  isBackstopConfigEmpty() && process.exit();
  const componentLabel = backstopConfig.scenarios[0].label;
  const componentUrl = backstopConfig.scenarios[0].url;
  httpreq.get(endpoints.fractalUrl, (err, res) => {
    if (err) {
      console.log(
        colors.red(
          'Please make sure Fractal server is started before running your tests'
        )
      );
      !scenarios && process.exit();
      return;
    }
    httpreq.get(componentUrl, (err, res) => {
      if (res.statusCode === 404) {
        console.log(
          colors.red(
            `The variant ${componentLabel} is not a valid variant of the ${componentLabel.substring(
              0,
              componentLabel.indexOf('-')
            )} component`
          )
        );
        process.exit();
        return;
      }
    });
    backstopRef(scenarios).then(() => {
      backstopTest(scenarios, callback);
    });
  });
};
