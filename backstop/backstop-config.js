module.exports = {
  scenarios: [
    {
      label: 'accordion',
      cookiePath: './../backstop_data/engine_scripts/cookies.json',
      url: 'http://localhost:3004/components/preview/accordion',
      referenceUrl: 'http://localhost:4000/components/preview/accordion',
      readyEvent: '',
      readySelector: '',
      delay: 0,
      hideSelectors: [],
      removeSelectors: [],
      hoverSelector: '',
      clickSelector: '',
      postInteractionWait: 0,
      selectors: [],
      selectorExpansion: true,
      misMatchThreshold: 0.1,
      requireSameDimensions: true
    }
  ]
};
