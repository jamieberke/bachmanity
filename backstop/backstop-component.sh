#!/usr/bin/env node

source ./scripts/colours.sh
source ./scripts/paths.sh

function variantConfig {
  component=`sed 's/--.*//g' <<< $1`;

  if [ ! -d "${componentsPath}${component}" ]; then
    echo ${red}"Component $component does not exist in ${componentsPath}"${colEnd};
    exit
  fi
  echo "module.exports = {
  scenarios: [
    {
      'label': '$1',
      'cookiePath': './../backstop_data/engine_scripts/cookies.json',
      'url': 'http://localhost:3004/components/preview/$1',
      'referenceUrl': 'http://localhost:4000/components/preview/$1',
      'readyEvent': '',
      'readySelector': '',
      'delay': 0,
      'hideSelectors': [],
      'removeSelectors': [],
      'hoverSelector': '',
      'clickSelector': '',
      'postInteractionWait': 0,
      'selectors': [],
      'selectorExpansion': true,
      'misMatchThreshold': 0.1,
      'requireSameDimensions': true
    }
  ]
};" >> "${backstopConfigPath}"
}
function componentConfig {

  if [ ! -d "${componentsPath}${1}" ]; then
    echo ${red}"Component $1 does not exist in ${componentsPath}"${colEnd};
    exit
  fi

  declare -a files
  files=(${componentsPath}$1/*hbs)
  pos=$(( ${#files[*]} - 1 ))
  last=${files[$pos]}

  echo "module.exports = {
  scenarios: [" >> "${backstopConfigPath}"

  for FILE in "${files[@]}"
  do
    fileName=${FILE##*/}
    len=${#fileName}-4
    fileName=${fileName:0:$len}
    if [[ $FILE == $last ]]
    then
      echo "    {
      'label': '$fileName',
      'cookiePath': './../backstop_data/engine_scripts/cookies.json',
      'url': 'http://localhost:3004/components/preview/$fileName',
      'referenceUrl': 'http://localhost:4000/components/preview/$fileName',
      'readyEvent': '',
      'readySelector': '',
      'delay': 0,
      'hideSelectors': [],
      'removeSelectors': [],
      'hoverSelector': '',
      'clickSelector': '',
      'postInteractionWait': 0,
      'selectors': [],
      'selectorExpansion': true,
      'misMatchThreshold': 0.1,
      'requireSameDimensions': true
    }
  ]
};" >> "${backstopConfigPath}"
    else
      echo "    {
        'label': '$fileName',
        'cookiePath': './../backstop_data/engine_scripts/cookies.json',
        'url': 'http://localhost:3004/components/preview/$fileName',
        'referenceUrl': 'http://localhost:4000/components/preview/$fileName',
        'readyEvent': '',
        'readySelector': '',
        'delay': 0,
        'hideSelectors': [],
        'removeSelectors': [],
        'hoverSelector': '',
        'clickSelector': '',
        'postInteractionWait': 0,
        'selectors': [],
        'selectorExpansion': true,
        'misMatchThreshold': 0.1,
        'requireSameDimensions': true
      }," >> "${backstopConfigPath}"
    fi
  done
}