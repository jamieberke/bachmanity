const colors = require('colors');
const express = require('express');
const exphbs = require('express-handlebars');
const opn = require('opn');
const bodyParser = require('body-parser');
const app = express();
const endpoints = require('./../scripts/endpoints');
const backstop = require('./backstop');
const backstopScenarios = [];

/*
  Create a handlebars instance and setup the config
*/
const hbs = exphbs.create({
  extname: 'hbs',
  layoutsDir: `views`,
  defaultLayout: `${__dirname}/views/layouts/main`,
  partialsDir: `${__dirname}/views/partials`
});

/*
  Create an express server instance and run it
*/
const server = app.listen(8082, res => {
  const port = server.address().port;
  opn(`http://localhost:${port}/components`);
  console.log(
    colors.green(`Backstop app listening at http://localhost:${port}`)
  );
});

/*
  Mounts the specified middleware function or functions at the specified path:
  the middleware function is executed when the base of the requested path matches path.
*/
app.use(express.static(`${__dirname}./../public`));
app.use(express.static(`${__dirname}./../backstop_data`));
app.use(express.static(`${__dirname}`));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

/*
  Registers the given template engine callback as ext.
*/
app.engine('hbs', hbs.engine);

/*
  Assigns setting name to value. You may store any value that you want,
  but certain names can be used to configure the behavior of the server.
*/
app.set('views', `${__dirname}/views/`);
app.set('view engine', 'hbs');

/*
  Returns the value of name app setting, where name is one of the strings in the app settings table.
*/
app.get('/exit', (req, res) => {
  process.exit(0);
});
app.get('/components', (req, res) => {
  backstop.getFractalComponents(components => {
    res.render('components', {
      componentsList: components
    });
  });
});

/*
  Routes HTTP POST requests to the specified path with the specified callback functions.
*/
app.post('/', (req, res) => {
  for (const key in req.body) {
    if (Object.prototype.hasOwnProperty.call(req.body, key)) {
      backstopScenarios.push({
        label: `${key}`,
        cookiePath: './../backstop_data/engine_scripts/cookies.json',
        url: `${endpoints.fractalPreviewUrl}${key}`,
        referenceUrl: `${endpoints.designPreviewUrl}${key}`,
        readyEvent: '',
        readySelector: '',
        delay: 0,
        hideSelectors: [],
        removeSelectors: [],
        hoverSelector: '',
        clickSelector: '',
        postInteractionWait: 0,
        selectors: [],
        selectorExpansion: true,
        misMatchThreshold: 0.1,
        requireSameDimensions: true
      });
    }
  }
  backstop.backstopRun(backstopScenarios, () => {
    res.render('results', {
      components: req.body
    });
  });
});
