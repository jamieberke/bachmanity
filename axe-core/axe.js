const colors = require('colors');
const paths = require('./paths');
const envUrl = process.env.npm_package_config_envUrl;
const axeBuilder = require('axe-webdriverjs');
const webDriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const handlebars = require('handlebars');
const fs = require('fs');
const mkdirp = require('mkdirp');
const axeRules = require('./axe.config');
let ruleList = [];

function renderToString(source, axeResults, url, key) {
  const data = {
    url,
    passes: axeResults.passes,
    fails: axeResults.violations
  };
  const template = handlebars.compile(source);
  const outputString = template(data);
  mkdirp('./axe-core/results', err => {
    if (err) {
      console.log(colors.red(`Could not create /results directory`));
    } else {
      console.log(colors.green(`Directory ./results created`));
      fs.writeFile(
        `${__dirname}/results/results_${key}.html`,
        outputString,
        err => {
          if (err) throw err;
          console.log(
            colors.green(
              `File saved to ${__dirname}/results/results_${key}.html`
            )
          );
        }
      );
    }
  });
  return outputString;
}

axeRules.forEach(rule => {
  rule.enabled && ruleList.push(rule.id);
});

const driver = new webDriver.Builder()
  .forBrowser('chrome')
  .setChromeOptions(new chrome.Options().headless())
  .build();

module.exports.init = () => {
  try {
    paths.config.forEach(path => {
      const url = `${path.ssl ? 'https://' : 'http://'}${envUrl}${path.path}`;
      axeRunner(url, path.key);
    });
  } catch (error) {
    console.log(
      colors.red(
        `Cannot read valid array of objects from ${__dirname}/paths.js`
      )
    );
  }
};

const axeRunner = (url, key) => {
  driver.get(url).then(data => {
    axeBuilder(driver)
      .withRules(ruleList)
      .withTags(['wcag2a', 'wcag2aa'])
      .analyze(results => {
        fs.readFile(`${__dirname}/results.hbs`, (err, data) => {
          if (!err) {
            const source = data.toString();
            renderToString(source, results, url, key);
          } else {
            console.log(
              colors.red(
                `${__dirname}/results.hbs file does not exist. Please create it.`
              )
            );
          }
        });
      });
  });
};
