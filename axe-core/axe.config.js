/*
  The default operation for axe.run is to run all WCAG 2.0 Level A and Level AA rules. If you want to
  disable individual rules then please set them to `true`
*/
module.exports = [
  {
    description: 'ensure every accesskey attribute value is unique',
    id: 'accesskeys',
    enabled: true
  },
  {
    description: 'ensure <area> elements of image maps have alternate text',
    id: 'area-alt',
    enabled: true
  },
  {
    description: 'ensure ARIA attributes are allowed for an elements role',
    id: 'aria-allowed-attr',
    enabled: true
  },
  {
    description:
      'ensure unsupported DPUB roles are only used on elements with implicit fallback roles',
    id: 'aria-dpub-role-fallback',
    enabled: true
  },
  {
    description:
      'ensure aria-hidden="true" is not present on the document body.',
    id: 'aria-hidden-body',
    enabled: true
  },
  {
    description:
      'ensure elements with ARIA roles have all required ARIA attributes',
    id: 'aria-required-attr',
    enabled: true
  },
  {
    description:
      'ensure elements with an ARIA role that require child roles contain them',
    id: 'aria-required-children',
    enabled: true
  },
  {
    description:
      'ensure elements with an ARIA role that require parent roles are contained by them',
    id: 'aria-required-parent',
    enabled: true
  },
  {
    description: 'ensure all elements with a role attribute use a valid value',
    id: 'aria-roles',
    enabled: true
  },
  {
    description: 'ensure all ARIA attributes have valid values',
    id: 'aria-valid-attr-value',
    enabled: true
  },
  {
    description:
      'ensure attributes that begin with aria- are valid ARIA attributes',
    id: 'aria-valid-attr',
    enabled: true
  },
  {
    description: 'ensure <audio> elements have captions',
    id: 'audio-caption',
    enabled: true
  },
  {
    description: 'ensure <blink> elements are not used',
    id: 'blink',
    enabled: true
  },
  {
    description: 'ensure buttons have discernible text',
    id: 'button-name',
    enabled: true
  },
  {
    description:
      'ensure each page has at least one mechanism for a user to bypass navigation and jump straight to the content',
    id: 'bypass',
    enabled: true
  },
  {
    description:
      'ensure related <input type="checkbox"> elements have a group and that that group designation is consistent',
    id: 'checkboxgroup',
    enabled: true
  },
  {
    description:
      'ensure the contrast between foreground and background colors meets WCAG 2 AA contrast ratio thresholds',
    id: 'color-contrast',
    enabled: true
  },
  {
    description: 'ensure <dl> elements are structured correctly',
    id: 'definition-list',
    enabled: true
  },
  {
    description: 'ensure <dt> and <dd> elements are contained by a <dl>',
    id: 'dlitem',
    enabled: true
  },
  {
    description:
      'ensure each HTML document contains a non-empty <title> element',
    id: 'document-title',
    enabled: true
  },
  {
    description: 'ensure every id attribute value is unique',
    id: 'duplicate-id',
    enabled: true
  },
  {
    description: 'ensure headings have discernible text',
    id: 'empty-heading',
    enabled: true
  },
  {
    description: 'ensure elements in the focus order have an appropriate role',
    id: 'focus-order-semantics',
    enabled: true
  },
  {
    description:
      'ensure <iframe> and <frame> elements contain the axe-core script',
    id: 'frame-tested',
    enabled: true
  },
  {
    description:
      'ensure <iframe> and <frame> elements contain a unique title attribute',
    id: 'frame-title-unique',
    enabled: true
  },
  {
    description:
      'ensure <iframe> and <frame> elements contain a non-empty title attribute',
    id: 'frame-title',
    enabled: true
  },
  {
    description: 'ensure the order of headings is semantically correct',
    id: 'heading-order',
    enabled: true
  },
  {
    description: 'Informs users about hidden content.',
    id: 'hidden-content',
    enabled: true
  },
  {
    description: 'ensure every HTML document has a lang attribute',
    id: 'html-has-lang',
    enabled: true
  },
  {
    description:
      'ensure the lang attribute of the <html> element has a valid value',
    id: 'html-lang-valid',
    enabled: true
  },
  {
    description:
      'ensure <img> elements have alternate text or a role of none or presentation',
    id: 'image-alt',
    enabled: true
  },
  {
    description:
      'ensure button and link text is not repeated as image alternative',
    id: 'image-redundant-alt',
    enabled: true
  },
  {
    description: 'ensure <input type="image"> elements have alternate text',
    id: 'input-image-alt',
    enabled: true
  },
  {
    description:
      'ensure that every form element is not solely labeled using the title or aria-describedby attributes',
    id: 'label-title-only',
    enabled: true
  },
  {
    description: 'ensure every form element has a label',
    id: 'label',
    enabled: true
  },
  {
    description:
      'The banner landmark should not be contained in another landmark',
    id: 'landmark-banner-is-top-level',
    enabled: true
  },
  {
    description:
      'The contentinfo landmark should not be contained in another landmark',
    id: 'landmark-contentinfo-is-top-level',
    enabled: true
  },
  {
    description:
      'The main landmark should not be contained in another landmark',
    id: 'landmark-main-is-top-level',
    enabled: true
  },
  {
    description: 'ensure the document has no more than one banner landmark',
    id: 'landmark-no-duplicate-banner',
    enabled: true
  },
  {
    description:
      'ensure the document has no more than one contentinfo landmark',
    id: 'landmark-no-duplicate-contentinfo',
    enabled: true
  },
  {
    description:
      'ensure a navigation point to the primary content of the page. If the page contains iframes, each iframe should contain either no main landmarks or just one',
    id: 'landmark-one-main',
    enabled: true
  },
  {
    description:
      'ensure presentational <table> elements do not use <th>, <caption> elements or the summary attribute',
    id: 'layout-table',
    enabled: true
  },
  {
    description: 'Links can be distinguished without relying on color',
    id: 'link-in-text-block',
    enabled: true
  },
  {
    description: 'ensure links have discernible text',
    id: 'link-name',
    enabled: true
  },
  {
    description: 'ensure that lists are structured correctly',
    id: 'list',
    enabled: true
  },
  {
    description: 'ensure <li> elements are used semantically',
    id: 'listitem',
    enabled: true
  },
  {
    description: 'ensure <marquee> elements are not used',
    id: 'marquee',
    enabled: true
  },
  {
    description: 'ensure <meta http-equiv="refresh"> is not used',
    id: 'meta-refresh',
    enabled: true
  },
  {
    description: 'ensure <meta name="viewport"> can scale a significant amount',
    id: 'meta-viewport-large',
    enabled: true
  },
  {
    description:
      'ensure <meta name="viewport"> does not disable text scaling and zooming',
    id: 'meta-viewport',
    enabled: true
  },
  {
    description: 'ensure <object> elements have alternate text',
    id: 'object-alt',
    enabled: true
  },
  {
    description: 'ensure p elements are not used to style headings',
    id: 'p-as-heading',
    enabled: true
  },
  {
    description:
      'ensure that the page, or at least one of its frames contains a level-one heading',
    id: 'page-has-heading-one',
    enabled: true
  },
  {
    description:
      'ensure related <input type="radio"> elements have a group and that the group designation is consistent',
    id: 'radiogroup',
    enabled: true
  },
  {
    description: 'ensure all content is contained within a landmark region',
    id: 'region',
    enabled: true
  },
  {
    description: 'ensure the scope attribute is used correctly on tables',
    id: 'scope-attr-valid',
    enabled: true
  },
  {
    description: 'ensure that server-side image maps are not used',
    id: 'server-side-image-map',
    enabled: true
  },
  {
    description: 'ensure all skip links have a focusable target',
    id: 'skip-link',
    enabled: true
  },
  {
    description: 'ensure tabindex attribute values are not greater than 0',
    id: 'tabindex',
    enabled: true
  },
  {
    description: 'ensure that tables do not have the same summary and caption',
    id: 'table-duplicate-name',
    enabled: true
  },
  {
    description: 'ensure that tables with a caption use the <caption> element.',
    id: 'table-fake-caption',
    enabled: true
  },
  {
    description:
      'ensure that each non-empty data cell in a large table has one or more table headers',
    id: 'td-has-header',
    enabled: true
  },
  {
    description:
      'ensure that each cell in a table using the headers refers to another cell in that table',
    id: 'td-headers-attr',
    enabled: true
  },
  {
    description:
      'ensure that each table header in a data table refers to data cells',
    id: 'th-has-data-cells',
    enabled: true
  },
  {
    description: 'ensure lang attributes have valid values',
    id: 'valid-lang',
    enabled: true
  },
  {
    description: 'ensure <video> elements have captions',
    id: 'video-caption',
    enabled: true
  },
  {
    description: 'ensure <video> elements have audio descriptions',
    id: 'video-description',
    enabled: true
  }
];
