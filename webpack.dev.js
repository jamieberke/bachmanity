const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const execSync = require('child_process').execSync;

const status = execSync('git status -s');
if (status.length > 0) {
  console.warn('Uncommited changed detected');
}

module.exports = merge(common, {
  devtool: 'inline-sourcemap',
  watch: true
});
