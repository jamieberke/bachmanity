#!/bin/bash

cp -R ./scripts/hooks/* .git/hooks
find .git/hooks -type f -exec chmod 777 {} \;
