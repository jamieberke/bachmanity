module.exports = {
  fractalUrl: 'http://localhost:3004/',
  designUrl: 'http://localhost:4000/',
  fractalPreviewUrl: 'http://localhost:3004/components/preview/',
  designPreviewUrl: 'http://localhost:4000/components/preview/'
};
